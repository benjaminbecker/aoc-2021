import dataclasses
import enum
import math
import statistics
from typing import Iterable
import re
from collections import deque

from data import path_data_file


def depth_increases(d1: int, d2: int) -> bool:
    return d2 > d1


def sliding_window(
    numbers: list[int],
    width: int
) -> Iterable[list[int]]:
    index_start = range(len(numbers) - width + 1)
    result = (
        numbers[i_start:i_start + width]
        for i_start in index_start
    )
    return result


def sum_windows(
    numbers: list[int],
    width: int,
):
    return [
        sum(_) for _ in sliding_window(numbers, width)
    ]


def count_increases(depths: Iterable[int]) -> int:
    pairs = zip(depths[:-1], depths[1:])
    return sum(depth_increases(*pair) for pair in pairs)


def read_strings_from_file(filename):
    with open(path_data_file(filename)) as f:
        return list(_ for _ in f)


def read_ints_from_file(filename):
    with open(path_data_file(filename)) as f:
        return list(int(_) for _ in f)


def read_binary_ints_from_file(filename):
    with open(path_data_file(filename)) as f:
        return list(int(_, 2) for _ in f)


def read_bingo_from_file(filename):
    with open(path_data_file(filename)) as f:
        draws = [int(_) for _ in f.readline().split(",")]

        f.readline()

        boards = []
        current_board = []
        for line in f:
            if line == "\n":
                boards.append(current_board)
                current_board = []
            else:
                current_board.extend(int(_) for _ in line.split())
        if len(current_board):
            boards.append(current_board)

    return draws, boards


def read_hydrothermal_vent_points_from_file(filename):
    line_definitions = tuple()
    with open(path_data_file(filename)) as f:
        for line in f:
            start, _, end = line.split()
            x_start, y_start = tuple(int(_) for _ in start.split(","))
            x_end, y_end = tuple(int(_) for _ in end.split(","))
            line_def = ((x_start, y_start), (x_end, y_end))
            line_definitions = (*line_definitions, line_def)
    return line_definitions


def read_csv_single_line(filename):
    with open(path_data_file(filename)) as f:
        return list(int(_) for _ in f.readline().split(","))


def parse_submarine_command(command):
    m = re.match(r"(\w*) (\d*)", command)
    return m.group(1), int(m.group(2))


def apply_submarine_command(horizontal, depth, aim, command):
    direction, amount = parse_submarine_command(command)
    match direction:
        case "forward":
            return horizontal + amount, depth + aim * amount, aim
        case "down":
            return horizontal, depth, aim + amount
        case "up":
            return horizontal, depth, aim - amount


def final_position(commands):
    horizontal, depth, aim = 0, 0, 0
    for command in commands:
        horizontal, depth, aim = apply_submarine_command(
            horizontal,
            depth,
            aim,
            command
        )
    return horizontal, depth


def get_bit(number, exponent):
    return (number >> exponent) & 1


def most_common_bits(numbers, number_of_bits):
    count_ones = [0 for _ in range(number_of_bits)]
    for n in numbers:
        for i in range(number_of_bits):
            bit = get_bit(n, i)
            if bit == 1:
                count_ones[i] += 1
            else:
                count_ones[i] -= 1

    result_bits = [
        1 if _ >= 0 else 0
        for _ in count_ones
    ]

    result = sum(
        bit << position
        for position, bit in enumerate(result_bits)
    )

    return result


def gamma_rate(numbers, number_of_bits=5):
    return most_common_bits(numbers, number_of_bits)


def epsilon_rate(gamma_rate, number_of_bits=5):
    return bit_flip(gamma_rate, number_of_bits)


def bit_flip(number, number_of_bits):
    return sum(1 << _ for _ in range(number_of_bits)) ^ number


class BitCriteria(enum.Enum):
    MOST_COMMON = enum.auto()
    LEAST_COMMON = enum.auto()


def oxygen_generator_rating(numbers, number_of_bits=5):
    return bit_filter(numbers, number_of_bits, bit_criteria=BitCriteria.MOST_COMMON)


def co2_scrubber_rating(numbers, number_of_bits=5):
    return bit_filter(numbers, number_of_bits, bit_criteria=BitCriteria.LEAST_COMMON)


def bit_filter(numbers, number_of_bits, bit_criteria=BitCriteria.MOST_COMMON):
    filtered_numbers = list(numbers)
    for exponent in range(number_of_bits - 1, -1, -1):
        filter_criteria = most_common_bits(filtered_numbers, number_of_bits)
        if bit_criteria == bit_criteria.LEAST_COMMON:
            filter_criteria = bit_flip(filter_criteria, number_of_bits)
        filtered_numbers = list(filter(
            lambda n: get_bit(n, exponent) == get_bit(filter_criteria, exponent),
            filtered_numbers
        ))
        if len(filtered_numbers) == 1:
            return filtered_numbers[0]


class Bingo:
    def __init__(self, board):
        N = 5
        assert len(board) == N * N
        self.board = board
        start_of_row = (_ * N for _ in range(N))
        self.rows = list(set(_ + start for _ in range(N)) for start in start_of_row)
        self.cols = list(set(row * N + col for row in range(N)) for col in range(N))

    def _marked_indices(self, drawn_numbers):
        return [i for i, x in enumerate(self.board) if x in drawn_numbers]

    def _has_row(self, indices):
        for row in self.rows:
            if row.issubset(indices):
                return True

    def _has_col(self, indices):
        for col in self.cols:
            if col.issubset(indices):
                return True

    def bingo(self, drawn_numbers):
        marked = self._marked_indices(drawn_numbers)
        return self._has_row(marked) or self._has_col(marked)

    def unmarked_numbers(self, drawn_numbers):
        return [x for x in self.board if x not in drawn_numbers]


def play_bingo(boards: list[Bingo], numbers):
    for n in range(len(numbers)):
        drawn_numbers = numbers[:n]
        for board in boards:
            if board.bingo(drawn_numbers):
                return sum(board.unmarked_numbers(drawn_numbers)) * drawn_numbers[-1]


def loose_bingo(boards: list[Bingo], numbers):
    number_of_boards = len(boards)
    winners = set()
    for n in range(len(numbers)):
        drawn_numbers = numbers[:n]
        for board in boards:
            if board.bingo(drawn_numbers):
                winners.add(board)
                if len(winners) == number_of_boards:
                    return sum(board.unmarked_numbers(drawn_numbers)) * drawn_numbers[-1]


class HydrothermalVentureDetector:
    def __init__(self, data, use_diagonals=False):
        self.data = data
        self.use_diagonals = use_diagonals

    def _render_horizontal(self, start, end, occupied):
        y = start[1]
        x_start, x_end = min(start[0], end[0]), max(start[0], end[0])
        for x in range(x_start, x_end + 1):
            current_count = occupied.get((x, y), 0)
            occupied[(x, y)] = current_count + 1

    def _render_vertical(self, start, end, occupied):
        x = start[0]
        y_start, y_end = min(start[1], end[1]), max(start[1], end[1])
        for y in range(y_start, y_end + 1):
            current_count = occupied.get((x, y), 0)
            occupied[(x, y)] = current_count + 1

    def _render_diagonal(self, start, end, occupied):
        direction_x = 1 if start[0] < end[0] else -1
        direction_y = 1 if start[1] < end[1] else -1
        iter_x = range(start[0], end[0] + direction_x, direction_x)
        iter_y = range(start[1], end[1] + direction_y, direction_y)
        for x, y in zip(iter_x, iter_y):
            current_count = occupied.get((x, y), 0)
            occupied[(x, y)] = current_count + 1

    def _is_horizontal(self, start, end):
        return start[1] == end[1]

    def _is_vertical(self, start, end):
        return start[0] == end[0]

    def _is_diagonal(self, start, end):
        return abs(end[0] - start[0]) == abs(end[1] - start[1])

    def render(self):
        occupied = dict()
        for line in self.data:
            start, end = line
            if self._is_horizontal(start, end):
                self._render_horizontal(start, end, occupied)
            elif self._is_vertical(start, end):
                self._render_vertical(start, end, occupied)
            elif self.use_diagonals and self._is_diagonal(start, end):
                self._render_diagonal(start, end, occupied)
        return occupied

    def number_of_overlaping_fields(self):
        occupied = self.render()
        return len([_ for _ in occupied.values() if _ > 1])


def lantern_fish_prognosis(initial_state: list[int], number_of_days):
    N = 9

    def create_histogram(state: list[int]):
        histogram = deque(
            (state.count(_) for _ in range(N)),
            maxlen=N
        )
        return histogram

    def update_histogram(histogram):
        histogram[6+1] += histogram[0]
        histogram.rotate(-1)

    histogram = create_histogram(initial_state)
    for _ in range(number_of_days):
        update_histogram(histogram)
    return sum(histogram)


@dataclasses.dataclass
class DisplayDataItem:
    wire_combinations: str
    output: str

    def __post_init__(self):
        self._wire_to_segment = None

    def identify_segments(self):
        combinations = self.wire_combinations.split(" ")
        one = set(next(_ for _ in combinations if len(_) == 2))
        four = set(next(_ for _ in combinations if len(_) == 4))
        seven = set(next(_ for _ in combinations if len(_) == 3))
        eight = set(next(_ for _ in combinations if len(_) == 7))
        combinations_of_5 = [set(_) for _ in combinations if len(_) == 5]
        combinations_of_6 = [set(_) for _ in combinations if len(_) == 6]
        intersection_combinations_of_5 = set.intersection(*combinations_of_5)
        intersection_combinations_of_6 = set.intersection(*combinations_of_6)

        segment_to_wire = dict()  # {segment: wire}
        segment_to_wire["a"], = seven - one
        segment_to_wire["b"], = four - one - intersection_combinations_of_5
        segment_to_wire["c"], = seven - intersection_combinations_of_6
        segment_to_wire["d"], = intersection_combinations_of_5 - intersection_combinations_of_6
        segment_to_wire["e"], = ((eight - intersection_combinations_of_5) - one) - (four - seven)
        segment_to_wire["f"], = set.intersection(one, intersection_combinations_of_6)
        segment_to_wire["g"], = set("abcdefg") - set(segment_to_wire.values())

        wire_to_segment = {v: k for k, v in segment_to_wire.items()}
        return wire_to_segment

    @property
    def wire_to_segment(self):
        if self._wire_to_segment is None:
            self._wire_to_segment = self.identify_segments()
        return self._wire_to_segment

    def decode_digit(self, wires: str):
        segments = set(self.wire_to_segment[_] for _ in wires)
        digit_to_segments = {
            0: set("abcefg"),
            1: set("cf"),
            2: set("acdeg"),
            3: set("acdfg"),
            4: set("bcdf"),
            5: set("abdfg"),
            6: set("abdefg"),
            7: set("acf"),
            8: set("abcdefg"),
            9: set("abcdfg"),
        }
        return next(k for k, v in digit_to_segments.items() if v == segments)

    def decode_digits(self):
        all_wires = self.output.split(" ")
        return [self.decode_digit(_) for _ in all_wires]

    def output_number(self):
        digits = self.decode_digits()
        L = len(digits)
        result = sum(digit * 10 ** (L - _ - 1) for _, digit in enumerate(digits))
        return result

    def identify_wires(self):
        def identify_digits_by_number_of_segments(combination):
            number_of_segments = {
                # number_of_segments: digit
                2: 1,
                4: 4,
                3: 7,
                7: 8,
            }
            return number_of_segments.get(len(combination))

        def identify_digits(combination):
            return identify_digits_by_number_of_segments(combination)

        result = {comb: identify_digits(comb) for comb in self.output.split(" ")}
        result = {k: v for k, v in result.items() if v is not None}
        return result

    def count_1_4_7_8(self):
        output_split = self.output.split(" ")
        return sum(output_split.count(_) for _ in self.identify_wires().keys())


def read_7_segment_display_data_from_file(filename: str):
    with open(path_data_file(filename)) as f:
        lines = f.read().splitlines()
        display_data = [DisplayDataItem(*line.split(" | ")) for line in lines]
        return display_data


class Grid2D:
    def __init__(
        self,
        elevation: list[int],
        *,
        width: int,
        height: int
    ) -> None:
        self._elevation = elevation
        self._width = width
        self._height = height

    def elevation(self, x, y):
        if x >= self._width:
            raise ValueError(f"x must be smaller than width, {x = }, {self._width = }")
        if y >= self._height:
            raise ValueError(f"y must be smaller than height, {y = }, {self._width = }")
        index = y * self._width + x
        return self._elevation[index]

    def neighbour_coordinates(self, x, y):
        left = (x - 1, y) if x > 0 else None
        right = (x + 1, y) if x < self._width - 1 else None
        up = (x, y - 1) if y > 0 else None
        down = (x, y + 1) if y < self._height - 1 else None
        return [_ for _ in (up, right, down, left) if _ is not None]

    def neighbour_elevations(self, x, y):
        coords = self.neighbour_coordinates(x, y)
        return [self.elevation(*c) for c in coords]

    def is_low_point(self, x, y):
        elevation = self.elevation(x, y)
        neighbour_elevations = self.neighbour_elevations(x, y)
        return all(elevation < e for e in neighbour_elevations)

    @property
    def low_point_coords(self):
        result = set()
        for x in range(self._width):
            for y in range(self._height):
                if self.is_low_point(x, y):
                    result.add((x, y))
        return result

    def risk_level(self, x, y):
        if not self.is_low_point(x, y):
            raise ValueError(f"{(x, y) = } is not a low point. risk level is not defined")
        return self.elevation(x, y) + 1

    @property
    def sum_of_risk_levels(self):
        return sum(self.risk_level(*xy) for xy in self.low_point_coords)

    def basin_size(self, x_low, y_low):
        basin_coords = self.explore_basin(x_low, y_low)
        return len(basin_coords)

    def explore_basin(self, x, y, basin_coords=None):
        if basin_coords is None:
            basin_coords = {(x, y)}
        elevation = self.elevation(x, y)
        neighbour_coordinates = self.neighbour_coordinates(x, y)
        for coord in neighbour_coordinates:
            elevation_neighbour = self.elevation(*coord)
            if elevation < elevation_neighbour < 9:
                basin_coords.add(coord)
                basin_coords = self.explore_basin(*coord, basin_coords=basin_coords)
        return basin_coords

    def product_of_3_largest_basin_sizes(self):
        basin_sizes = (self.basin_size(*xy) for xy in self.low_point_coords)
        sorted_basin_sizes = sorted(basin_sizes, reverse=True)
        return math.prod(sorted_basin_sizes[:3])


def load_grid_data(filename: str):
    with open(path_data_file(filename)) as f:
        data = f.read()
    lines = data.splitlines()
    width, height = len(lines[0]), len(lines)
    lines_joined = "".join(lines)
    numbers = [int(_) for _ in lines_joined]
    grid = Grid2D(numbers, width=width, height=height)
    return grid


class NavSyntaxError:
    pass


@dataclasses.dataclass
class Corrupted:
    letter: str

    def __str__(self):
        return self.letter


@dataclasses.dataclass()
class Incomplete:
    stack: deque


class NavigationSubsystemCommandParser:
    def __init__(self, data: str):
        self._lines = data.splitlines()

    @property
    def pairs(self):
        return {
            "(": ")",
            "[": "]",
            "{": "}",
            "<": ">",
        }

    @property
    def opening(self):
        return set(self.pairs.keys())

    @property
    def closing(self):
        return set(self.pairs.values())

    def validate_navigation_syntax(self, line: str):
        stack = deque()
        for letter in line:
            if letter in self.opening:
                stack.append(self.pairs[letter])
            elif letter in self.closing:
                try:
                    popped_letter = stack.pop()
                except IndexError:
                    return None
                if popped_letter != letter:
                    return Corrupted(letter)
        if len(stack) > 0:
            return Incomplete(stack)

    def count_syntax_errors(self):
        errors = (self.validate_navigation_syntax(line) for line in self._lines)
        errors = (_ for _ in errors if not isinstance(_, Incomplete))
        errors = [str(_) for _ in errors if _ is not None]
        error_counts = {
            letter: errors.count(letter)
            for letter in ")]}>"
        }
        return error_counts

    def rate_syntax_errors(self):
        points = {
            ")": 3,
            "]": 57,
            "}": 1197,
            ">": 25137,
        }
        return sum(
            points[letter] * amount
            for letter, amount in self.count_syntax_errors().items()
        )

    def autocomplete(self):
        errors = (self.validate_navigation_syntax(line) for line in self._lines)
        completion = {
            index: "".join(reversed(err.stack))
            for index, err in enumerate(errors)
            if isinstance(err, Incomplete)
        }
        return completion

    def rate_autocomplete(self):
        completions = self.autocomplete().values()

        def rate_single(completion):
            points = {
                ")": 1,
                "]": 2,
                "}": 3,
                ">": 4,
            }
            result = 0
            for letter in completion:
                result *= 5
                result += points[letter]
            return result

        ratings = (rate_single(comp) for comp in completions)
        return statistics.median_low(ratings)


def read_navigation_subsystem_commands_from_file(filename):
    with open(path_data_file(filename)) as f:
        data = f.read()
    return NavigationSubsystemCommandParser(data)
