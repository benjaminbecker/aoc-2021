import dataclasses
import unittest

from lib import *


class TestCaseDay1(unittest.TestCase):
    def setUp(self) -> None:
        self.depths = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263, ]

    def test_count_increases(self):
        self.assertEqual(7, count_increases(self.depths))

    def test_sliding_window(self):
        self.assertEqual(
            [[0, 1, 2], [1, 2, 3], [2, 3, 4], [3, 4, 5], [4, 5, 6]],
            list(sliding_window(list(range(7)), 3))
        )

    def test_sum_windows(self):
        self.assertEqual(
            [3, 6, 9, 12, 15],
            sum_windows(list(range(7)), 3)
        )
        self.assertEqual(
            5,
            count_increases(
                sum_windows(self.depths, 3)
            )
        )


class TestCaseDay2(unittest.TestCase):
    def test_final_position(self):
        commands = [
            "forward 5",
            "down 5",
            "forward 8",
            "up 3",
            "down 8",
            "forward 2",
        ]
        self.assertEqual(
            (15, 60),
            final_position(commands)
        )


class TestCaseDay3(unittest.TestCase):

    def setUp(self) -> None:
        self.data = [
            0b00100,
            0b11110,
            0b10110,
            0b10111,
            0b10101,
            0b01111,
            0b00111,
            0b11100,
            0b10000,
            0b11001,
            0b00010,
            0b01010,
        ]

    def test_most_common_bits(self):
        self.assertEqual(
            0b10110,
            gamma_rate(self.data)
        )

    def test_epsilon_rate(self):
        self.assertEqual(
            0b01001,
            epsilon_rate(0b10110),
        )

    def test_oxygen_generator_rating(self):
        self.assertEqual(
            0b10111,
            oxygen_generator_rating(self.data)
        )

    def test_co2_scrubber_rating(self):
        self.assertEqual(
            0b01010,
            co2_scrubber_rating(self.data)
        )


class TestCaseDay4(unittest.TestCase):
    def test_bingo(self):
        player = Bingo([
            22, 13, 17, 11, 0,
            8, 2, 23, 4, 24,
            21, 9, 14, 16, 7,
            6, 10, 3, 18, 5,
            1, 12, 20, 15, 19
        ]
        )
        self.assertFalse(player.bingo((1, 2, 3)))
        self.assertTrue(player.bingo((1, 2, 8, 23, 4, 24, 100)))
        self.assertTrue(player.bingo((0, 24, 7, 5, 19, 1)))
        self.assertTrue(player.bingo((1, 12, 20, 15, 19)))
        self.assertTrue(player.bingo((23, 17, 3, 14, 20)))

    def test_import(self):
        draws, board_numbers = read_bingo_from_file("day4.txt")
        print(draws)
        print(board_numbers[1])

    def test_play_bingo(self):
        draws, board_numbers = read_bingo_from_file("day4.test.txt")
        boards = [Bingo(_) for _ in board_numbers]
        winner = play_bingo(boards, draws)
        self.assertEqual(
            4512,
            winner
        )

    def test_loose_bingo(self):
        draws, board_numbers = read_bingo_from_file("day4.test.txt")
        boards = [Bingo(_) for _ in board_numbers]
        looser = loose_bingo(boards, draws)
        self.assertEqual(
            1924,
            looser
        )


class TestCaseDay5(unittest.TestCase):

    def setUp(self) -> None:
        self.data = (
            ((0, 9), (5, 9)),
            ((8, 0), (0, 8)),
            ((9, 4), (3, 4)),
            ((2, 2), (2, 1)),
            ((7, 0), (7, 4)),
            ((6, 4), (2, 0)),
            ((0, 9), (2, 9)),
            ((3, 4), (1, 4)),
            ((0, 0), (8, 8)),
            ((5, 5), (8, 2)),
        )

    def test_import(self):
        self.assertEqual(
            self.data,
            read_hydrothermal_vent_points_from_file("day5.test.txt")
        )

    def test_counting(self):
        self.assertEqual(
            5,
            HydrothermalVentureDetector(self.data).number_of_overlaping_fields()
        )

    def test_counting_including_diagonals(self):
        self.assertEqual(
            12,
            HydrothermalVentureDetector(self.data, use_diagonals=True).number_of_overlaping_fields()
        )


class TestCaseDay6(unittest.TestCase):
    def test_prognosis(self):
        initial_state = [3, 4, 3, 1, 2]
        self.assertEqual(
            26,
            lantern_fish_prognosis(initial_state, 18)
        )
        self.assertEqual(
            5934,
            lantern_fish_prognosis(initial_state, 80)
        )
        self.assertEqual(
            26984457539,
            lantern_fish_prognosis(initial_state, 256)
        )


class TestCaseDay8(unittest.TestCase):
    def test_decryption(self):
        display_data = read_7_segment_display_data_from_file("day8.test.txt")
        print(display_data[0].identify_segments())
        self.assertEqual(
            26,
            sum(_.count_1_4_7_8() for _ in display_data)
        )
        self.assertEqual(
            61229,
            sum(_.output_number() for _ in display_data)
        )

    def test_single_line(self):
        diplay_data_item = DisplayDataItem(
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab",
            "cdfeb fcadb cdfeb cdbaf"
        )
        self.assertEqual(
            [5, 3, 5, 3],
            diplay_data_item.decode_digits()
        )


class TestCaseDay9(unittest.TestCase):
    def test_it_detects_low_points(self):
        grid = load_grid_data("day9.test.txt")
        self.assertEqual(
            8,
            grid.elevation(2, 1)
        )
        self.assertSetEqual(
            {(1, 0), (9, 0), (2, 2), (6, 4)},
            grid.low_point_coords
        )
        self.assertEqual(
            15,
            grid.sum_of_risk_levels
        )

    def test_it_calculates_cluster_size(self):
        grid = load_grid_data("day9.test.txt")
        self.assertEqual(3, grid.basin_size(1, 0))
        self.assertEqual(9, grid.basin_size(9, 0))
        self.assertEqual(14, grid.basin_size(2, 2))
        self.assertEqual(9, grid.basin_size(6, 4))

    def test_product_of_3_largest_basin_sizes(self):
        grid = load_grid_data("day9.test.txt")
        self.assertEqual(
            1134,
            grid.product_of_3_largest_basin_sizes()
        )


class TestCaseDay10(unittest.TestCase):
    def setUp(self) -> None:
        self.parser = read_navigation_subsystem_commands_from_file("day10.test.txt")

    def test_it_returns_true_if_valid(self):
        self.assertIsNone(
            NavigationSubsystemCommandParser("").validate_navigation_syntax("()")
        )

    def test_it_returns_false_for_corrupted_line(self):
        self.assertEqual(
            Corrupted("}"),
            NavigationSubsystemCommandParser("").validate_navigation_syntax("((<<>>)}")
        )

    def test_count_syntax_errors(self):
        self.assertDictEqual(
            {")": 2, "]": 1, "}": 1, ">": 1},
            self.parser.count_syntax_errors()
        )

    def test_rate_syntax_errors(self):
        self.assertEqual(
            26397,
            self.parser.rate_syntax_errors()
        )

    def test_autocomplete(self):
        self.assertDictEqual(
            {
                0: "}}]])})]",
                1: ")}>]})",
                3: "}}>}>))))",
                6: "]]}}]}]}>",
                9: "])}>",
            },
            self.parser.autocomplete()
        )

    def test_score_autocomplete(self):
        self.assertEqual(
            288957,
            self.parser.rate_autocomplete()
        )


if __name__ == '__main__':
    unittest.main()
