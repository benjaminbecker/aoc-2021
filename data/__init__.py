import pathlib


folder_data = pathlib.Path(__file__).parent


def path_data_file(filename):
    return folder_data.joinpath(filename)
