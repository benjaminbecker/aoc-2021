import lib


def day_1():
    data = lib.read_ints_from_file("day1.txt")
    print(lib.count_increases(data))
    print(lib.count_increases(
        lib.sum_windows(data, 3)
    ))


def day_2():
    commands = lib.read_strings_from_file("day2.txt")
    h, d = lib.final_position(commands)
    print(h*d)


def day_3():
    numbers = lib.read_binary_ints_from_file("day3.txt")
    gamma_rate = lib.gamma_rate(numbers, 12)
    epsilon_rate = lib.epsilon_rate(gamma_rate, 12)
    oxygen_generator_rating = lib.oxygen_generator_rating(numbers, 12)
    co2_scrubber_rating = lib.co2_scrubber_rating(numbers, 12)
    print(f"{gamma_rate * epsilon_rate = }")
    print(f"{oxygen_generator_rating * co2_scrubber_rating = }")


def day_4():
    draws, board_numbers = lib.read_bingo_from_file("day4.txt")
    boards = [lib.Bingo(_) for _ in board_numbers]
    winner = lib.play_bingo(boards, draws)
    looser = lib.loose_bingo(boards, draws)
    print(f"{winner = }")
    print(f"{looser = }")


def day_5():
    line_definitions = lib.read_hydrothermal_vent_points_from_file("day5.txt")
    print(f"{lib.HydrothermalVentureDetector(line_definitions).number_of_overlaping_fields() = }")
    print(f"{lib.HydrothermalVentureDetector(line_definitions, use_diagonals=True).number_of_overlaping_fields() = }")


def day_6():
    initial_state = lib.read_csv_single_line("day6.txt")
    print(f"{lib.lantern_fish_prognosis(initial_state, 80) = }")
    print(f"{lib.lantern_fish_prognosis(initial_state, 256) = }")


def day_8():
    display_data = lib.read_7_segment_display_data_from_file("day8.txt")
    print(f"{sum(_.count_1_4_7_8() for _ in display_data) = }")
    print(f"{sum(_.output_number() for _ in display_data) = }")


def day_9():
    grid = lib.load_grid_data("day9.txt")
    print(f"{grid.sum_of_risk_levels = }")
    print(f"{grid.product_of_3_largest_basin_sizes() = }")


def day_10():
    parser = lib.read_navigation_subsystem_commands_from_file("day10.txt")
    print(f"{parser.rate_syntax_errors() = }")
    print(f"{parser.rate_autocomplete() = }")


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    day_1()
    day_2()
    day_3()
    day_4()
    day_5()
    day_6()
    day_8()
    day_9()
    day_10()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
